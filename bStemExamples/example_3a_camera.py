# =============================================================================
#
# COPYRIGHT 2014 Brain Corporation.
# All rights reserved. Brain Corporation proprietary and confidential.
#
# The party receiving this software directly from Brain Corporation ( the
# "Recipient" ) may use this software and make copies thereof as reasonably
# necessary solely for the purposes set forth in the agreement between the
# Recipient and Brain Corporation (the "Agreement"). The software may be
# used in source code form solely by the Recipient's employees. The Recipient
# shall have no right to sublicense, assign, transfer or otherwise provide the
# source code to any third party. Subject to the terms and conditions set
# forth in the Agreement, this software, in binary form only, may be
# distributed by the Recipient to its customers. Brain Corporation retains all
# ownership rights in and to the software.
#
# This notice shall supercede any other notices contained within the software.
# =============================================================================

"""
Example: bstem Cameras
  Updated 1/21/2014
"""

"""
This tutorial shows how to take snapshots and record videos using the stereo cameras on bSTEM.
Accessing cameras on bStem requires a software daemon (mm-qcamera-daemon) to run in the background.
For the default login (ubuntu) this is started automatically
when you login (see /etc/profile.d/bstemrc.sh).

This example demonstrates use of the camera API module. It displays video
taken from both cameras and allows JPEG capture and recording of the left
video into an avi file.

Before running this tutorial, please confirm that both cameras are plugged in (not just one camera).
Then restart the board. You may simply turn it on and off. Then run this tutorial.
"""

import cv2
from bstem import camera


def camera_example(num_frames=50, cam=None):

    print "\nSelect the left or right camera window\n\
     - press space bar to take snapshot\n\
     - press 'c' to start/stop video recording\n\
     - press 'q' to quit\n\
    You can take multiple snapshots and multiple video sequences.\n\n"

    if cam is None:
        example_cam = camera.Camera()
    else:
        example_cam = cam

    im = example_cam.snapshot()[0]
    cv2.namedWindow('img_left')
    cv2.namedWindow('img_right')

    height = im.shape[0]
    width = im.shape[1]
    is_recording = False
    video_left = None

    video_count = 0
    snapshot_count = 0
    cnt = 0

    while True:
        img_left, img_right = example_cam.snapshot()
        cv2.imshow('img_left', img_left)
        cv2.imshow('img_right', img_right)

        if is_recording:
            video_left.write(img_left)

        key = cv2.waitKey(10)

        if key == ord(' '):
            cv2.imwrite('img_left_' + str(snapshot_count) + '.jpg', img_left)
            print 'img_left_' + str(snapshot_count) + '.jpg saved'

            cv2.imwrite('img_right_' + str(snapshot_count) + '.jpg', img_right)
            print 'img_right_' + str(snapshot_count) + '.jpg saved'

            snapshot_count += 1

        elif key == ord('c'):
            if is_recording:
                print 'Finish recording video_left_' + str(video_count) + '.avi'
                video_count += 1
            else:
                video_left = cv2.VideoWriter('video_left_' + str(video_count) + '.avi',
                                    cv2.cv.CV_FOURCC('F', 'M', 'P', '4'), 15,
                                    (width, height), isColor=True)

                print 'Start recording video_left_' + str(video_count) + '.avi'
            is_recording = not is_recording

        elif key == ord('q'):
            break

        cnt += 1
        if cnt > num_frames:
            break

    if cam is None:
        example_cam.cleanup()

    cv2.destroyAllWindows()

if __name__ == "__main__":
    camera_example(num_frames=float("inf"))
