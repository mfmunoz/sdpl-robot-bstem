# =============================================================================
#
# COPYRIGHT 2014 Brain Corporation.
# All rights reserved. Brain Corporation proprietary and confidential.
#
# The party receiving this software directly from Brain Corporation ( the
# "Recipient" ) may use this software and make copies thereof as reasonably
# necessary solely for the purposes set forth in the agreement between the
# Recipient and Brain Corporation (the "Agreement"). The software may be
# used in source code form solely by the Recipient's employees. The Recipient
# shall have no right to sublicense, assign, transfer or otherwise provide the
# source code to any third party. Subject to the terms and conditions set
# forth in the Agreement, this software, in binary form only, may be
# distributed by the Recipient to its customers. Brain Corporation retains all
# ownership rights in and to the software.
#
# This notice shall supercede any other notices contained within the software.
# =============================================================================

"""
Example: bstem Sensors
  Updated 1/21/2014
"""

"""
The bStem is equipped with a wide variety of sensors, including an accelerometer, gyroscope, barometer and magnetometer.
All sensors are accessible via the Bstem class from the bstem.platform module.
This example demonstrates use of the sensor API module. It reads and displays
values from each of the sensors on the bstem (Accelormeter, Gyroscope,
Barometer, Magnetometer).
"""

# standard access
from bstem.platform import Bstem
# alternative access
from bstem.sensor import Accelerometer, Gyroscope, Barometer, Magnetometer

from time import sleep, ctime


def bstem_sensor_examples():
    '''
        Shows the standard way to access the sensors (through Bstem platform class)
    '''

    b = Bstem()

    # retrieve last recorded set of x, y and z values
    (x, y, z) = b.accelerometer.value

    # retrieve last recorded values individually
    x = b.accelerometer.x
    y = b.accelerometer.y
    z = b.accelerometer.z

    print 'Individually retrieved accelerometer values, x: ', x, ' y: ', y, ' z: ', z, '\n'

    (x, y, z) = b.gyroscope.value

    x = b.gyroscope.x
    y = b.gyroscope.y
    z = b.gyroscope.z

    print 'Individually retrieved gyroscope values, x: ', x, ' y: ', y, ' z: ', z, '\n'

    (x, y, z) = b.magnetometer.value

    x = b.magnetometer.x
    y = b.magnetometer.y
    z = b.magnetometer.z

    print 'Individually retrieved magnetometer values, x: ', x, ' y: ', y, ' z: ', z, '\n'

    pressure = b.barometer.pressure

    print 'Retrieved barometer value: ', pressure, '\n'

    if b.accelerometer.enabled:
        b.accelerometer.enabled = False

    if not b.gyroscope.enabled:
        b.gyroscope.enabled = True


def bstem_alternate_sensor_examples():
    '''
        Shows an alternative way to access the sensors, through individual sensor APIs
    '''

    # Accelerometer

    acc = Accelerometer()

    print 'Monitoring accelerometer:'

    for i in range(10):
        # retrieve last recorded set of x, y and z values
        (x, y, z) = acc.value
        print ctime(), 'x: ', x, ' y: ', y, ' z: ', z
        sleep(0.5)

    # retrieve last recorded values individually
    x = acc.x
    y = acc.y
    z = acc.z
    print 'Individually retrieved values, x: ', x, ' y: ', y, ' z: ', z, '\n'

    # Gyroscope

    gyro = Gyroscope()

    # check if gyroscope enabled
    if not gyro.enabled:
        gyro.enabled = True

    print 'Monitoring gyroscope:'

    for i in range(10):
        (x, y, z) = gyro.value
        print ctime(), 'x: ', x, ' y: ', y, ' z: ', z
        sleep(0.5)

    x = gyro.x
    y = gyro.y
    z = gyro.z
    print 'Individually retrieved values, x: ', x, ' y: ', y, ' z: ', z, '\n'

    #disable gyroscope
    gyro.enabled = True

    # Barometer
    bar = Barometer()

    if not bar.enabled:
        bar.enabled = True

    print 'Monitoring barometer:'

    for i in range(10):
        pressure = bar.pressure
        print ctime(), 'pressure: ', pressure
        sleep(0.5)

    bar.enabled = True

    print

    # Magnetometer

    mag = Magnetometer()

    # check if gyroscope enabled
    if not mag.enabled:
        mag.enabled = True

    print 'Monitoring magnetometer:'

    for i in range(10):
        (x, y, z) = mag.value
        print ctime(), 'x: ', x, ' y: ', y, ' z: ', z
        sleep(0.5)

    x = mag.x
    y = mag.y
    z = mag.z
    print 'Individually retrieved values, x: ', x, ' y: ', y, ' z: ', z

    mag.enabled = False


if __name__ == "__main__":
    bstem_sensor_examples()
