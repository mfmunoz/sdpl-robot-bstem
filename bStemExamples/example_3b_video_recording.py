# =============================================================================
#
# COPYRIGHT 2014 Brain Corporation.
# All rights reserved. Brain Corporation proprietary and confidential.
#
# The party receiving this software directly from Brain Corporation ( the
# "Recipient" ) may use this software and make copies thereof as reasonably
# necessary solely for the purposes set forth in the agreement between the
# Recipient and Brain Corporation (the "Agreement"). The software may be
# used in source code form solely by the Recipient's employees. The Recipient
# shall have no right to sublicense, assign, transfer or otherwise provide the
# source code to any third party. Subject to the terms and conditions set
# forth in the Agreement, this software, in binary form only, may be
# distributed by the Recipient to its customers. Brain Corporation retains all
# ownership rights in and to the software.
#
# This notice shall supercede any other notices contained within the software.
# =============================================================================

"""
Example: bstem Video Recording
  Updated 1/21/2014
"""

"""
This tutorial shows how to take snapshots and record videos using the stereo cameras on bSTEM.
Accessing cameras on bStem requires a software daemon (mm-qcamera-daemon) to run in the background.
For the default login (ubuntu) this is started automatically when you login (see /etc/rc.local).

This example demonstrates the gstreamer-based recording function.
example_3_camera.py takes snapshots in a loop and writes the sequence of
snapshots into a video, in which case it is hard to control the frame rate.

Before running this tutorial, please confirm that both cameras are plugged in (not just one camera).
Then restart the board. You may simply turn it on and off. Then run this tutorial.
"""

from bstem import camera
import time
import sys


def video_recording_example(duration, file_name, video_format='mjpeg', cam=None):
    '''
    A simple example demonstrates the recording capability.

    To play back use the command:

    >> gst-launch filesrc location=[filename] ! decodebin ! ffmpegcolorspace ! ximagesink
    '''

    if cam is None:
        # video_format= 'mjpeg' or 'omx'. jpeg_quality=[0-100] for jpeg
        example_cam = camera.Camera(video_format=video_format, jpeg_quality=80)
    else:
        example_cam = cam

    example_cam.start_recording(file_name=file_name)
    time.sleep(duration)
    example_cam.end_recording()

    if cam is None:
        example_cam.cleanup()

if __name__ == "__main__":
    if len(sys.argv) != 3:
        duration = 5
        file_name = 'example_video.avi'
        print('Usage: python example_4_video.py <duration in seconds> <output file>\n')
        print('[Default] duration = %d sec' % duration)
        print('[Default] output file = %s' % file_name)
    else:
        duration = int(sys.argv[1])
        file_name = sys.argv[2]
    video_recording_example(duration, file_name)
