# =============================================================================
#
# COPYRIGHT 2014 Brain Corporation.
# All rights reserved. Brain Corporation proprietary and confidential.
#
# The party receiving this software directly from Brain Corporation ( the
# "Recipient" ) may use this software and make copies thereof as reasonably
# necessary solely for the purposes set forth in the agreement between the
# Recipient and Brain Corporation (the "Agreement"). The software may be
# used in source code form solely by the Recipient's employees. The Recipient
# shall have no right to sublicense, assign, transfer or otherwise provide the
# source code to any third party. Subject to the terms and conditions set
# forth in the Agreement, this software, in binary form only, may be
# distributed by the Recipient to its customers. Brain Corporation retains all
# ownership rights in and to the software.
#
# This notice shall supercede any other notices contained within the software.
# =============================================================================

"""
Example: bstem LEDs
  Updated 1/21/2014
"""

"""
Hello World. This example flashes each of the colored LEDs on the
bstem (blue, green, yellow).
This tutorial provides a brief introduction to Python programming for the bSTEM.
Interfacing with the bSTEM hardware can be done through Python using the bstem package.
"""

from bstem.platform import Bstem
from time import sleep


def led_example():
    bstem = Bstem()

    for x in range(10):

        # turn LEDs off
        bstem.blue_led.brightness = 0
        bstem.green_led.brightness = 0
        bstem.yellow_led.brightness = 0
        sleep(.5)

        # turn LEDs on
        bstem.blue_led.brightness = 1
        bstem.green_led.brightness = 1
        bstem.yellow_led.brightness = 1
        sleep(.5)

        # turn LEDs on (max brightness)
        bstem.blue_led.brightness = 2
        bstem.green_led.brightness = 2
        bstem.yellow_led.brightness = 2
        sleep(.5)

    print 'Success!'


if __name__ == "__main__":
    led_example()
