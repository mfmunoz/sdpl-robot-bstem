# =============================================================================
#
# COPYRIGHT 2014 Brain Corporation.
# All rights reserved. Brain Corporation proprietary and confidential.
#
# The party receiving this software directly from Brain Corporation ( the
# "Recipient" ) may use this software and make copies thereof as reasonably
# necessary solely for the purposes set forth in the agreement between the
# Recipient and Brain Corporation (the "Agreement"). The software may be
# used in source code form solely by the Recipient's employees. The Recipient
# shall have no right to sublicense, assign, transfer or otherwise provide the
# source code to any third party. Subject to the terms and conditions set
# forth in the Agreement, this software, in binary form only, may be
# distributed by the Recipient to its customers. Brain Corporation retains all
# ownership rights in and to the software.
#
# This notice shall supercede any other notices contained within the software.
# =============================================================================

"""
Example: bstem camera config
  Updated 2/5/2014
"""

"""
This tutorial shows how to change the settings (brightness, saturation, hue, ..) on
the stereo cameras on bSTEM.
"""

import cv2
from bstem.camera import Camera
from bstem.platform import Bstem


def show_video(cam):
    '''
    Show 20ms of video.
    '''

    for _ in range(20):
        [img_left, img_right] = cam.snapshot()
        cv2.imshow('img_left', img_left)
        cv2.imshow('img_right', img_right)
        cv2.waitKey(1)


def camera_config_example():
    '''
    Configure each of the properties of the camera in turn.
    '''

    example_cam = Camera()
    b = Bstem()

    cv2.namedWindow('img_left')
    cv2.namedWindow('img_right')

    show_video(example_cam)

    # Brightness

    df_brightness = b.camera.brightness
    print 'default brightness ', df_brightness

    for i in range(0, 21, 5):
        b.camera.brightness = i
        print ' brightness ', b.camera.brightness
        show_video(example_cam)

    b.camera.brightness = df_brightness

    # Contrast

    df_contrast = b.camera.contrast
    print 'default contrast ', df_contrast

    for i in range(16, 65, 16):
        b.camera.contrast = i
        print ' contrast ', b.camera.contrast
        show_video(example_cam)

    b.camera.contrast = df_contrast

    # Saturation

    df_saturation = b.camera.saturation
    print 'default saturation ', df_saturation

    for i in range(0, 256, 17):
        b.camera.saturation = i
        print ' saturation ', b.camera.saturation
        show_video(example_cam)

    b.camera.saturation = df_saturation

    # Sharpness

    df_sharpness = b.camera.sharpness
    print 'default sharpness ', df_sharpness

    for i in range(0, 8):
        b.camera.sharpness = i
        print ' sharpness ', b.camera.sharpness
        show_video(example_cam)

    b.camera.sharpness = df_sharpness

    # Hue

    df_hue = b.camera.hue
    print 'default hue ', df_hue

    for i in range(-22, 23, 11):
        b.camera.hue = i
        print ' hue ', b.camera.hue
        show_video(example_cam)

    b.camera.hue = df_hue

    # Fade-to-black

    df_fade_to_black = b.camera.fade_to_black
    print 'default fade to black ', df_fade_to_black

    for i in range(0, 33, 8):
        b.camera.fade_to_black = i
        print ' fade to black ', b.camera.fade_to_black
        show_video(example_cam)

    b.camera.fade_to_black = df_fade_to_black

    # Sfx

    df_sfx = b.camera.sfx
    print 'default sfx ', df_sfx
    sfx_modes = ['disabled', 'monochrome', 'sepia', 'negative',
                 'solarization', 'solarization_neg_uv']

    for mode in sfx_modes:
        b.camera.sfx = mode
        print ' sfx ', b.camera.sfx
        show_video(example_cam)

    b.camera.sfx = df_sfx

    example_cam.cleanup()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    camera_config_example()
