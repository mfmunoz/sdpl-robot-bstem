# =============================================================================
#
# COPYRIGHT 2014 Brain Corporation.
# All rights reserved. Brain Corporation proprietary and confidential.
#
# The party receiving this software directly from Brain Corporation ( the
# "Recipient" ) may use this software and make copies thereof as reasonably
# necessary solely for the purposes set forth in the agreement between the
# Recipient and Brain Corporation (the "Agreement"). The software may be
# used in source code form solely by the Recipient's employees. The Recipient
# shall have no right to sublicense, assign, transfer or otherwise provide the
# source code to any third party. Subject to the terms and conditions set
# forth in the Agreement, this software, in binary form only, may be
# distributed by the Recipient to its customers. Brain Corporation retains all
# ownership rights in and to the software.
#
# This notice shall supercede any other notices contained within the software.
# =============================================================================

"""
Example: bSTEM Control (Scheduler and ControlLoop)
  Updated 1/21/2014
"""

"""
This example demonstrates use of the control API to schedule three tasks:
one that interates the accelemer, another the flashes an LED, and a third that
just waits, simulating slow code that blocks the processor.

To add a repeating behavior to your bSTEM project, make a new object
that inherits from ControlLoop and add code to it's' loop() function.
The Scheduler manages multiple loops.  It will execute each respective
loop() method at the specified frequency.

FAQ:
Q: How do I run the scheduler indefinitely?
A: Call Scheduler.start() with no arguments.  To halt execution, use Ctrl-C.
"""

from bstem.control import Scheduler, ControlLoop
from bstem.sensor import Accelerometer
from bstem.device import Led
from time import sleep


class Acceler(ControlLoop):
    """Reads and integrates the accelerometer"""
    def __init__(self, freq):
        self.meter = Accelerometer()
        self.sum = 0
        super(Acceler, self).__init__(freq)

    def loop(self):
        print 'A: ' + str(self.iter) + '__ '
        self.sum += self.meter.x


class Blinker(ControlLoop):
    """Turns on and off an LED"""
    def __init__(self, freq):
        self.led = Led("blue")
        self.on = False
        self.led.brightness = 0
        super(Blinker, self).__init__(freq)

    def loop(self):
        #switch state
        self.on = not self.on
        print 'B: _' + str(self.iter) + '_ ON:' + str(self.on)

        if self.on:
            self.led.brightness = 1
        else:
            self.led.brightness = 0


class Clunker(ControlLoop):
    """simulates  slow code by waiting"""
    def __init__(self, freq, compute_time):
        self.compute_time = compute_time
        super(Clunker, self).__init__(freq)

    def loop(self):
        print 'C: __' + str(self.iter)
        sleep(self.compute_time)


def timed_loop_example():

    print('Tasks A, B, and C will execute twice per second for 2 seconds.')

    # instantiate three objects, each for its own task
    a = Acceler(freq=2)
    b = Blinker(freq=2)
    c = Clunker(freq=2, compute_time=0.1)

    # add the objects to the scheduler
    Scheduler.add(a)
    Scheduler.add(b)
    Scheduler.add(c)

    # Run Scheduler for 2 seconds
    Scheduler.start(2)

    print('Scheduler stopped.')

    # avg_exec_time returns time in seconds
    print("Execution Time (in milliseconds) [A,B,C]: ")
    print(str(a.avg_exec_time * 1000),
          str(b.avg_exec_time * 1000),
          str(c.avg_exec_time * 1000))

    Scheduler.remove(a)
    Scheduler.remove(b)
    Scheduler.remove(c)

if __name__ == '__main__':
    timed_loop_example()
