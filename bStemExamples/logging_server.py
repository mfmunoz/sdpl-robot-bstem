# =============================================================================
#
# COPYRIGHT 2014 Brain Corporation.
# All rights reserved. Brain Corporation proprietary and confidential.
#
# The party receiving this software directly from Brain Corporation ( the
# "Recipient" ) may use this software and make copies thereof as reasonably
# necessary solely for the purposes set forth in the agreement between the
# Recipient and Brain Corporation (the "Agreement"). The software may be
# used in source code form solely by the Recipient's employees. The Recipient
# shall have no right to sublicense, assign, transfer or otherwise provide the
# source code to any third party. Subject to the terms and conditions set
# forth in the Agreement, this software, in binary form only, may be
# distributed by the Recipient to its customers. Brain Corporation retains all
# ownership rights in and to the software.
#
# This notice shall supercede any other notices contained within the software.
# =============================================================================

"""
Example: bSTEM Remote Logging [on PC]
  Updated 1/21/2014
"""

"""
bStem supports data logging both locally to a file, and to a another machine to
allow analysis and plotting of data remotely.

This example demonstrates use of the logging API to record data to a remote machine
(server side)
"""

from bstem.log import LogServer


def remotelogger_server(port):

    lsvr = LogServer(port)

    return lsvr


def print_logserver_data(logserver, fields):
    """
    Helper function to print data from a logserver.

    :param logserver: Server containing data to be printed
    :type logserver: LogServer instance
    :param fields: Fields (Sensor names) to print
    :type fields: String or Iterarble of Strings
    """

    if isinstance(fields, str):
        fields = (fields,)

    number_of_readings = len(logserver[fields[0]])

    print(', '.join(fields))

    for i in range(number_of_readings):
        ith_data = [str(logserver[sensor][i]) for sensor in fields]
        print(', '.join(ith_data))

if __name__ == "__main__":
    print 'This code would be executed on the server. See example_5b_server_client_logging.py.'
