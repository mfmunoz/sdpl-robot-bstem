# =============================================================================
#
# COPYRIGHT 2014 Brain Corporation.
# All rights reserved. Brain Corporation proprietary and confidential.
#
# The party receiving this software directly from Brain Corporation ( the
# "Recipient" ) may use this software and make copies thereof as reasonably
# necessary solely for the purposes set forth in the agreement between the
# Recipient and Brain Corporation (the "Agreement"). The software may be
# used in source code form solely by the Recipient's employees. The Recipient
# shall have no right to sublicense, assign, transfer or otherwise provide the
# source code to any third party. Subject to the terms and conditions set
# forth in the Agreement, this software, in binary form only, may be
# distributed by the Recipient to its customers. Brain Corporation retains all
# ownership rights in and to the software.
#
# This notice shall supercede any other notices contained within the software.
# =============================================================================

from bstem.log import VariableServer, VariableClient
from bstem.control import Scheduler

"""
Example: bstem parameter tuning with VariableClient
  Updated 1/21/2014
"""

"""
The VariableServer and VariableClient interface allows the value of variables within an object
to be set while the control loop is running, either on the bSTEM or remotely.
"""

# Define communication IP and ports for respective examples
LOCALHOST_IP = '127.0.0.1'
PORT_LOGSERVER_WTS = 1618
PORT_LOGSERVER_VS = 1620


def variable_server_example():

    # Set up VariableServer with 5Hz refresh at designated port
    vs = VariableServer(2, PORT_LOGSERVER_VS)

    # Define an object to register with the variable server
    class sample_object(object):
        def __init__(self):
            self.property_a = 0
            self.property_b = 2

    object_instance = sample_object()

    print('object_instance.property_a: ' + str(object_instance.property_a))
    print('object_instance.property_b: ' + str(object_instance.property_b))

    # "Register" 'object_instance' with the VariableServer as 'remote_object'
    vs.register(object_instance, 'remote_object')

    # Set up Variable client to interface with VariableServer
    vc = VariableClient(LOCALHOST_IP, PORT_LOGSERVER_VS)

    print('Setting property_a to 1 and property_b to 3')

    # Set remote_object's properties using VariableClient's 'set' method
    vc.set('remote_object.property_a', 1)
    vc.set('remote_object.property_b', 3)

    # Run scheduler loop for 2 seconds
    Scheduler.start(2)

    print('object_instance.property_a: ' + str(object_instance.property_a))
    print('object_instance.property_b: ' + str(object_instance.property_b))

    print('\n'),

    vs.close()


def main():
    variable_server_example()


if __name__ == '__main__':
    main()
