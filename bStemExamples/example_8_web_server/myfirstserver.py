# =============================================================================
#
# COPYRIGHT 2014 Brain Corporation.
# All rights reserved. Brain Corporation proprietary and confidential.
#
# The party receiving this software directly from Brain Corporation ( the
# "Recipient" ) may use this software and make copies thereof as reasonably
# necessary solely for the purposes set forth in the agreement between the
# Recipient and Brain Corporation (the "Agreement"). The software may be
# used in source code form solely by the Recipient's employees. The Recipient
# shall have no right to sublicense, assign, transfer or otherwise provide the
# source code to any third party. Subject to the terms and conditions set
# forth in the Agreement, this software, in binary form only, may be
# distributed by the Recipient to its customers. Brain Corporation retains all
# ownership rights in and to the software.
#
# This notice shall supercede any other notices contained within the software.
# =============================================================================

from flask import Flask, render_template, request, Response
import time
from bstem.device import Led
from bstem.sensor import Accelerometer


"""
Example: bstem web server
  Updated 1/21/2014
"""

"""
Flask is an extremely light-weight web application framework with an extensive plugin library.
It is a simple, yet useful, tool for interfacing with a bstem over a network.
This tutorial will walk you through the steps necessary to host a minimal Flask-powered web interface for the bstem.
Our web interface will have two features: 1. A toggle button for an on-board LED and 2. A continuous sensor readout.
"""


app = Flask(__name__)


@app.route("/")
def hello():
    return render_template("front_page.html")


@app.route("/toggle_led", methods=["POST"])
def toggle_led():
    if led.brightness:
        led.brightness = 0
    else:
        led.brightness = 2
    return request.data  # required at end of POST route


# Accelerometer Generator
def event_stream_acc():
    while True:
        time.sleep(0.25)
        yield "data: %s\n\n" % str(acc.z)


@app.route("/stream_acc")
def stream_acc():
    return Response(event_stream_acc(), mimetype="text/event-stream")

if __name__ == "__main__":

    try:
        led = Led("blue")
        led.enabled = True
    except:
        print "LED not found.  Try running as superuser"
        exit()

    try:
        acc = Accelerometer()
        acc.enabled = True
    except:
        print "Accelerometer not found.  Try running as superuser"
        exit()

    app.run(host="0.0.0.0", debug=True, threaded=True)
