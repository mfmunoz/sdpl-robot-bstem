# =============================================================================
#
# COPYRIGHT 2014 Brain Corporation.
# All rights reserved. Brain Corporation proprietary and confidential.
#
# The party receiving this software directly from Brain Corporation ( the
# "Recipient" ) may use this software and make copies thereof as reasonably
# necessary solely for the purposes set forth in the agreement between the
# Recipient and Brain Corporation (the "Agreement"). The software may be
# used in source code form solely by the Recipient's employees. The Recipient
# shall have no right to sublicense, assign, transfer or otherwise provide the
# source code to any third party. Subject to the terms and conditions set
# forth in the Agreement, this software, in binary form only, may be
# distributed by the Recipient to its customers. Brain Corporation retains all
# ownership rights in and to the software.
#
# This notice shall supercede any other notices contained within the software.
# =============================================================================

"""
Example: bSTEM Remote Plotting
  Updated 1/21/2014
"""

"""
The purpose of this tutorial is to learn how to plot data from the bSTEM.
It has been divided into two sections, one for viewing the plot by directly
connecting the bSTEM to a monitor (local plotting) and the other for sending
data from the bSTEM over a network to view the plot from another device
such as a laptop (remote plotting).

It is recommended that you read the local plotting section (example 6a) first as it
explains the basic aspects of plotting in more detail.

This example demonstrates use of the local plotting API to live plot data on the bstem from
a scheduled task.
"""

from bstem.plot import Plot
from bstem.control import Scheduler

from math import sin
from time import time


def main():

    plot_rainbow_example()
    plot_local_example()


def plot_local_example():

    # A plot's data is updated by repeatedly calling its 'func' argument and
    #  appending the returned result to the plot.  We define a function called
    #  'update_plot_fxn' that returns the most recent sensor reading.  We will
    #  pass it as the 'func' argument when we create a Line Plot or Histogram.
    def update_lineplot_fxn():
        # Line Plot can plot multiple fields
        return (sin(time() % 6.28) + .3, sin(time() % 6.28), sin(time() % 6.28) - .3)

    def update_histogram_fxn():
        # Histograms plot one field
        return sin(time() % 6.28)

    """ Create and display a Line Plot """
    # Add lineplot to Scheduler
    Plot.lineplot(update_lineplot_fxn, 'Local Line Plot')
    # Run Scheduler for 5 seconds
    Scheduler.start(5)
    # Remove lineplot from Scheduler
    Plot.clear()

    """ Create and display a Histogram. """
    Plot.histogram(update_histogram_fxn, 'Local Histogram')
    Scheduler.start(5)

    Plot.clear()

    """ Create and display a Histogram and Line Plot simultaneously """
    Plot.lineplot(update_lineplot_fxn, 'Local Line Plot')
    Plot.histogram(update_histogram_fxn, 'Local Histogram')
    Scheduler.start(5)

    Plot.clear()


def plot_rainbow_example():

    # A plot's data is updated by repeatedly calling its 'func' argument and
    #  appending the returned result to the plot.  We define a function called
    #  'update_plot_fxn' that returns the most recent sensor reading.  We will
    #  pass it as the 'func' argument when we create a Line Plot or Histogram.
    def update_lineplot_fxn():
        # Line Plots can plot multiple fields
        t = time()
        return (sin(t % 6.2830) + .6,
                sin(t % 6.2830) + .4,
                sin(t % 6.2830) + .2,
                sin(t % 6.2830),
                sin(t % 6.2830) - .2,
                sin(t % 6.2830) - .4,
                sin(t % 6.2830) - .6,)

    """ Create and display rainbow Line Plot """
    # Add Line Plot to Scheduler
    Plot.lineplot(update_lineplot_fxn, 'Rainbow', 'y_label', 'y_units', 'k',
                  ['m', 'r', 'y', 'g', 'c', 'b', 'w'], timewindow=12.56, linewidth=17)
    # Run Scheduler for 10 seconds
    Scheduler.start(10)
    # Remove Line Plot from Scheduler
    Plot.clear()

if __name__ == '__main__':
    main()
