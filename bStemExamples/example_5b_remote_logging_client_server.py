# =============================================================================
#
# COPYRIGHT 2014 Brain Corporation.
# All rights reserved. Brain Corporation proprietary and confidential.
#
# The party receiving this software directly from Brain Corporation ( the
# "Recipient" ) may use this software and make copies thereof as reasonably
# necessary solely for the purposes set forth in the agreement between the
# Recipient and Brain Corporation (the "Agreement"). The software may be
# used in source code form solely by the Recipient's employees. The Recipient
# shall have no right to sublicense, assign, transfer or otherwise provide the
# source code to any third party. Subject to the terms and conditions set
# forth in the Agreement, this software, in binary form only, may be
# distributed by the Recipient to its customers. Brain Corporation retains all
# ownership rights in and to the software.
#
# This notice shall supercede any other notices contained within the software.
# =============================================================================

"""
Example: bSTEM Remote Logging [on bstem]
  Updated 1/21/2014
"""

"""
bStem supports data logging both locally to a file, and to a another machine
to allow analysis and plotting of data remotely.

This example demonstrates use of the logging API to record data to a remote
machine.
"""

from bstem.control import Scheduler
from logging_client import remotelogger_client
from logging_server import remotelogger_server, print_logserver_data


def main():
    ip = '127.0.0.1'
    port = 1619

    # client and server are run in one script in this example
    rlgr_client = remotelogger_client(ip, port)

    # server is also run on same bstem in this example,
    # but could be run remotely on another system
    rlgr_server = remotelogger_server(port)

    # client and server run for 10 seconds
    Scheduler.start(10)

    # print data gathered on remote server
    print_logserver_data(rlgr_server, ("time", "acc_x", "gyro_x"))

    rlgr_client.close()
    rlgr_server.close()

if __name__ == "__main__":
    main()
