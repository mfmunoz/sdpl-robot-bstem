# =============================================================================
#
# COPYRIGHT 2014 Brain Corporation.
# All rights reserved. Brain Corporation proprietary and confidential.
#
# The party receiving this software directly from Brain Corporation ( the
# "Recipient" ) may use this software and make copies thereof as reasonably
# necessary solely for the purposes set forth in the agreement between the
# Recipient and Brain Corporation (the "Agreement"). The software may be
# used in source code form solely by the Recipient's employees. The Recipient
# shall have no right to sublicense, assign, transfer or otherwise provide the
# source code to any third party. Subject to the terms and conditions set
# forth in the Agreement, this software, in binary form only, may be
# distributed by the Recipient to its customers. Brain Corporation retains all
# ownership rights in and to the software.
#
# This notice shall supercede any other notices contained within the software.
# =============================================================================

"""
Example: bSTEM Remote Plotting [on bstem]
  Updated 1/21/2014
"""

"""
The purpose of this tutorial is to learn how to plot data from the bSTEM.
It has been divided into two sections, one for viewing the plot by directly
connecting the bSTEM to a monitor (local plotting) and the other for sending
data from the bSTEM over a network to view the plot from another device
such as a laptop (remote plotting).

It is recommended that you read the local plotting section (example 6a) first as it
explains the basic aspects of plotting in more detail.

This example demonstrates use of the remote plotting API to live plot data to a \
remote machine (client side and server side). Client-side code is in remoteplot_client.py,
and server-side code is in remoteplot_server.py

"""

from bstem.control import Scheduler
from bstem.plot import Plot

from remoteplot_client import remoteplot_client
from remoteplot_server import remoteplot_server


def main():
    ip = '127.0.0.1'
    # Use an unused port
    port = 2718

    rl = remoteplot_client(ip, port)

    # normally this would be run on a remote system
    # for this example, both the client and server
    # are run on the same local bstem.
    ls = remoteplot_server(port)

    # Run Scheduler for 10 seconds
    Scheduler.start(10)

    rl.close()

    # Clear plots from internal (static) plot queue
    Plot.clear()

    ls.close()


if __name__ == "__main__":
    main()
