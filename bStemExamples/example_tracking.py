# =============================================================================
#
# COPYRIGHT 2014 Brain Corporation.
# All rights reserved. Brain Corporation proprietary and confidential.
#
# The party receiving this software directly from Brain Corporation ( the
# "Recipient" ) may use this software and make copies thereof as reasonably
# necessary solely for the purposes set forth in the agreement between the
# Recipient and Brain Corporation (the "Agreement"). The software may be
# used in source code form solely by the Recipient's employees. The Recipient
# shall have no right to sublicense, assign, transfer or otherwise provide the
# source code to any third party. Subject to the terms and conditions set
# forth in the Agreement, this software, in binary form only, may be
# distributed by the Recipient to its customers. Brain Corporation retains all
# ownership rights in and to the software.
#
# This notice shall supercede any other notices contained within the software.
# =============================================================================

import cv2
from bstem.camera import Camera
from bstem.tracker import CAMShiftTracker
import time

"""
Example: bstem tracking
  Updated 1/21/2014
"""

"""
This tutorial shows how to use a color histogram based tracker
to track a user specified target from the video feed of bSTEM.
"""


class TrackerApp():
    def __init__(self):

        self.cam = Camera()

        self.img_curr = self._get_image()
        self.img_curr = cv2.cvtColor(self.img_curr, cv2.COLOR_RGB2BGR)

        cv2.imshow('Tracker', self.img_curr)

        cv2.setMouseCallback('Tracker', self._on_mouse)
        self.is_setting_target = False
        self.done_setting_target = False
        self.bb = [0, 0, 0, 0]
        self._get_initial_box()

        # Initialize the tracker
        self.tracker = CAMShiftTracker(self.bb, self.img_curr)

    def _get_image(self):
        # return left camera image
        image = self.cam.snapshot()[0]
        image = cv2.resize(image, None, fx=0.5, fy=0.5)
        return image

    def _on_mouse(self, event, x, y, flags, params):
        """
        Callback function for OpenCV mouse event
        """
        # click left-upper corner and drag to right-lower corner, the other way
        # will cause problem
        if event == cv2.EVENT_LBUTTONDOWN and not self.done_setting_target:
            self.bb[0] = x
            self.bb[1] = y
            self.is_setting_target = True

        elif event == cv2.EVENT_LBUTTONUP and not self.done_setting_target:
            self.is_setting_target = False
            self.done_setting_target = True

        elif event == cv2.EVENT_MOUSEMOVE and self.is_setting_target:
            self.bb[2] = x - self.bb[0]
            self.bb[3] = y - self.bb[1]

    def _get_initial_box(self):
        """
        Function for getting the initial bounding box
        """
        print 'Click and drag mouse to select the initial bounding box...'

        while True:
            self.img_curr = self._get_image()

            self.img_curr = cv2.cvtColor(self.img_curr, cv2.COLOR_RGB2BGR)

            self.img_curr_vis = self.img_curr.copy()
            if self.is_setting_target:
                cv2.rectangle(self.img_curr_vis, (self.bb[0], self.bb[1]),
                        (self.bb[0] + self.bb[2], self.bb[1] + self.bb[3]),
                         (0, 0, 255), thickness=2)

            cv2.imshow('Tracker', self.img_curr_vis)
            cv2.waitKey(50)

            if self.done_setting_target:
                break

    def run(self):
        """
        Run the tracker app
        """
        print "Press 'q' to quit"

        begin = time.time()
        fps = 0
        counter = 0
        while True:
            try:
                k = cv2.waitKey(20)
                if k == ord('q'):
                    break

                # Get a new frame
                self.img_curr = self._get_image()
                self.img_curr = cv2.cvtColor(self.img_curr, cv2.COLOR_RGB2BGR)

                # Update the tracker
                self.tracker.update(self.img_curr)

                # Display frame with the updated tracking box
                img_curr_vis = self.img_curr.copy()
                cv2.rectangle(img_curr_vis, (self.tracker.bb[0], self.tracker.bb[1]),
                    (self.tracker.bb[0] + self.tracker.bb[2], self.tracker.bb[1] + self.tracker.bb[3]),
                    (0, 0, 255), thickness=2)
                cv2.putText(img_curr_vis, 'fps:%2.1f' % fps,
                            (self.tracker.width - 100, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))

                if counter == 5:
                    fps = 5 / (time.time() - begin)
                    counter = 0
                    begin = time.time()

                counter += 1

                cv2.imshow("Tracker", img_curr_vis)

            except KeyboardInterrupt:
                break

        cv2.destroyAllWindows()

app = TrackerApp()
app.run()
