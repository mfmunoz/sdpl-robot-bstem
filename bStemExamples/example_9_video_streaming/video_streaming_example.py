# =============================================================================
#
# COPYRIGHT 2014 Brain Corporation.
# All rights reserved. Brain Corporation proprietary and confidential.
#
# The party receiving this software directly from Brain Corporation ( the
# "Recipient" ) may use this software and make copies thereof as reasonably
# necessary solely for the purposes set forth in the agreement between the
# Recipient and Brain Corporation (the "Agreement"). The software may be
# used in source code form solely by the Recipient's employees. The Recipient
# shall have no right to sublicense, assign, transfer or otherwise provide the
# source code to any third party. Subject to the terms and conditions set
# forth in the Agreement, this software, in binary form only, may be
# distributed by the Recipient to its customers. Brain Corporation retains all
# ownership rights in and to the software.
#
# This notice shall supercede any other notices contained within the software.
# =============================================================================

import cv2
import signal
import sys
import numpy as np

from flask import Flask, Response, render_template
from bstem.video import Video
from bstem.camera import Camera


"""
Example: bstem video streaming
  Updated 1/21/2014
"""

"""
bStem provides the ability to implement streaming video from the cameras to a local or remote web browser,
using the Flask and bStem Video and Camera packages.
"""


cam = Camera()


def video_callback():

    _, img = cam.snapshot()
    return img

vid = Video(video_callback)

app = Flask(__name__)


@app.route('/')
def index(name=None):
    vid.setup()
    return render_template('control.html')


@app.route('/mjpeg')
def mjpeg():
    return Response(mjpeg_stream(), mimetype='multipart/x-mixed-replace; boundary=--spionisto')


def mjpeg_stream():
    while vid.streaming:
        yield vid.next()


def shutdown(signal, frame):
    vid.shutdown()
    sys.exit(0)

if __name__ == '__main__':
    signal.signal(signal.SIGINT, shutdown)
    app.run(host='0.0.0.0', port=5000, debug=True)
