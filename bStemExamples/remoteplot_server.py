# =============================================================================
#
# COPYRIGHT 2014 Brain Corporation.
# All rights reserved. Brain Corporation proprietary and confidential.
#
# The party receiving this software directly from Brain Corporation ( the
# "Recipient" ) may use this software and make copies thereof as reasonably
# necessary solely for the purposes set forth in the agreement between the
# Recipient and Brain Corporation (the "Agreement"). The software may be
# used in source code form solely by the Recipient's employees. The Recipient
# shall have no right to sublicense, assign, transfer or otherwise provide the
# source code to any third party. Subject to the terms and conditions set
# forth in the Agreement, this software, in binary form only, may be
# distributed by the Recipient to its customers. Brain Corporation retains all
# ownership rights in and to the software.
#
# This notice shall supercede any other notices contained within the software.
# =============================================================================

from bstem.log import LogServer
from bstem.plot import Plot


def remoteplot_server(port):

    # LogServer is instantiated from the observing device
    ls = LogServer(port)

    # To use remote plotting, we pass a tuple to the plot's constructor
    #  (instead of an update function).  The tuple should have two elements:
    #  element 0 should be a LogServer and element 1 should be a tuple of
    #  strings, each representing a field to read on each update.  The
    #  structure of the return tuple will be the same as the structure of the
    #  field tuple passed in when the plot is created.

    """ Add a Histogram and Line Plot to Scheduler """
    # Add to Scheduler
    Plot.lineplot((ls, ('Sensor_A', 'Sensor_B', 'Sensor_C')), 'Remote Line Plot')
    Plot.histogram((ls, ('Sensor_A')), 'Remote Histogram')

    return ls

if __name__ == "__main__":
    print 'This code would be executed on the server. See example_6b_remote_plot_client_server.py.'
