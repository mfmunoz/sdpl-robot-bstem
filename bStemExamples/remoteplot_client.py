# =============================================================================
#
# COPYRIGHT 2014 Brain Corporation.
# All rights reserved. Brain Corporation proprietary and confidential.
#
# The party receiving this software directly from Brain Corporation ( the
# "Recipient" ) may use this software and make copies thereof as reasonably
# necessary solely for the purposes set forth in the agreement between the
# Recipient and Brain Corporation (the "Agreement"). The software may be
# used in source code form solely by the Recipient's employees. The Recipient
# shall have no right to sublicense, assign, transfer or otherwise provide the
# source code to any third party. Subject to the terms and conditions set
# forth in the Agreement, this software, in binary form only, may be
# distributed by the Recipient to its customers. Brain Corporation retains all
# ownership rights in and to the software.
#
# This notice shall supercede any other notices contained within the software.
# =============================================================================

from time import time
from math import sin
from bstem.log import RemoteLogger


def remoteplot_client(ip, port):

    # A function for generating fake sensor data
    def dummy_sensor_fxn():
        result_tuple = (sin(time() % 6.28) + .3,
                        sin(time() % 6.28),
                        sin(time() % 6.28) - .3)
        return result_tuple

    # The RemoteLogger would be instantiated on the bSTEM
    rl = RemoteLogger(ip,
                      dummy_sensor_fxn,
                      ('Sensor_A', 'Sensor_B', 'Sensor_C'),
                      20,
                      port)

    return rl

if __name__ == "__main__":
    print 'This code would be executed on the client. See example_6b_remote_plot_client_server.py.'
