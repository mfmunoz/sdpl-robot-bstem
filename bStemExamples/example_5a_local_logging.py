# =============================================================================
#
# COPYRIGHT 2014 Brain Corporation.
# All rights reserved. Brain Corporation proprietary and confidential.
#
# The party receiving this software directly from Brain Corporation ( the
# "Recipient" ) may use this software and make copies thereof as reasonably
# necessary solely for the purposes set forth in the agreement between the
# Recipient and Brain Corporation (the "Agreement"). The software may be
# used in source code form solely by the Recipient's employees. The Recipient
# shall have no right to sublicense, assign, transfer or otherwise provide the
# source code to any third party. Subject to the terms and conditions set
# forth in the Agreement, this software, in binary form only, may be
# distributed by the Recipient to its customers. Brain Corporation retains all
# ownership rights in and to the software.
#
# This notice shall supercede any other notices contained within the software.
# =============================================================================

"""
Example: bSTEM Logging
  Updated 1/21/2014
"""

from bstem.platform import Bstem
from bstem.log import Logger, FileLogger

from bstem.control import Scheduler

from time import time

"""
bStem supports data logging both locally to a file, and to a another machine to
allow analysis and plotting of data remotely.

This example demonstrates use of the logging API to record data on the bstem from
a scheduled task.

KEY TERMS:
Scheduler: Coordinates execution of parallel tasks
Logger: On-board bSTEM process that can write sensor data to CSV file
RemoteLogger: On-board bSTEM process that can transmit sensor data to a LogServer
LogServer: Remote (off of the bSTEM) process that receives sensor data from a RemoteLogger
VariableClient: Remote process for adjusting variables on the bSTEM
VariableServer: On-board bSTEM Server listening for VariableClient variable adjustments

FAQ:
Q: Why do I get an "[Errno 98] Address already in use" error?
A: LogServers and RemoteLoggers use sockets to communicate.  If this module is
     run while another process is already using a socket with the same protocol
     (at 127.0.0.1:1618), Errno 98 is thrown.  This commonly caused by running two
     instances of the same module.  To demonstrate, try executing this script
     twice in rapid succession.
Q: What is the difference between a Logger, FileLogger and RemoteLogger?
A: The difference is subtle, yet important.  A Logger will accumulate data in
     a on-board 'data' cache until we call one of its 'flush' methods:
     writeToCsv or writeToServer.  Calling either of these methods transmits
     the contents of the Logger's cache to a server or file for storage.  Like a
     Logger, RemoteLoggers and FileLoggers continually accumulate data to a
     'data' cache; however, unlike a Logger, RL's and FL's continually transmit
     data to their server/file.  Thus, RL's and FL's are more appropriate for
     applications that use bSTEM's real-time logging functionality.
"""


b = Bstem()


# A function for generating sensor data
def collect_data():
    return (time(), b.accelerometer.x, b.gyroscope.x)


def filelogger_example():

    # Start logging sensor data at 5 Hz
    flgr = FileLogger('/tmp/output.log',
                      collect_data,
                      ('time', 'acc_x', 'gyro_x'),
                      5)

    # Run scheduler loop for 2 seconds
    Scheduler.start(2)

    flgr.close()

    with open('/tmp/output.log', 'r') as f:
        for line in f:
            print(line),
    print('\n'),


def logger_writeToCsv_example():

    # Start logging sensor data at 5 Hz.
    lgr = Logger(collect_data,
                 ('time', 'acc_x', 'gyro_x'),
                 5)

    # Run scheduler loop for 2 seconds
    Scheduler.start(2)

    lgr.writeToCsv('/tmp/output.log')

    with open('/tmp/output.log', 'r') as f:
        for line in f:
            print(line),
    print('\n'),


def main():

    logger_writeToCsv_example()
    filelogger_example()


if __name__ == '__main__':
    main()
