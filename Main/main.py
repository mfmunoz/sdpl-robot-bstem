# =============================================================================
#
# 2015 San Diego Public Library and Robotics Group.
# All rights reserved, pending license selection.
#
# =============================================================================


# standard access
from bstem.platform import Bstem
# access to servos, gpio and motors
from bstem.platform import AdCord
# alternative access
from bstem.sensor import Accelerometer, Gyroscope, Barometer, Magnetometer
import time
from time import sleep, ctime

ad = AdCord()

def main():
